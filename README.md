# CodeIgniter Rest API for basic Login and Sign Up

A fully RESTful server implementation for CodeIgniter using one library, one
config file and one controller.

## Requirements

1. PHP 5.2 or greater
2. CodeIgniter 2.1.0 to 3.0-dev

## Installation

Set Up Database with provided .sql file.

Drag and drop the **application/libraries/Format.php** and **application/libraries/REST_Controller.php** files into your application's directories. Either autoload the `REST_Controller` class or `require_once` it at the top of your controllers to load it into the scope. Additionally, copy the **rest.php** file from **application/config** in your application's configuration directory. Also put the **application/controllers/api.php** in your application's controller dircetory.

## Handling Requests

When your controller extends from `REST_Controller`, the method names will be appended with the HTTP method used to access the request. If you're  making an HTTP `POST` call to `/login`, for instance, it would call a `Api#login_post()` method.

This allows you to implement a RESTful interface easily:

	class Api extends REST_Controller {

		public function login_post() {
			// process login		
		}
	
	}

## Content Types

`REST_Controller` supports a bunch of different request/response formats, including XML, JSON and serialised PHP. By default, the class will check the URL and look for a format either as an extension or as a separate segment.

This means your URLs can look like this:

	http://example.com/login.json
	http://example.com/login.xml

## Responses

The class provides a `response()` method that allows you to return data in the user's requested response format.

Returning any object / array / string / whatever is easy:

This will automatically return an `HTTP 200 OK` response. You can specify the status code in the second parameter:

	public function signup_post()
	{
		// ...create new user

		$this->response($user, 201); // Send an HTTP 201 Created
	}

If you don't specify a response code, and the data you respond with `== FALSE` (an empty array or string, for instance), the response code will automatically be set to `404 Not Found`:

	$this->response(array()); // HTTP 404 Not Found

## Multilingual Support

If your application uses language files to support multiple locales, `REST_Controller` will automatically parse the HTTP `Accept-Language` header and provide the language(s) in your actions. This information can be found in the `$this->response->lang` object:

	public function __construct()
	{
		parent::__construct();

		if (is_array($this->response->lang))
		{
			$this->load->language('application', $this->response->lang[0]);
		}
		else
		{
			$this->load->language('application', $this->response->lang);
		}
	}

## Authentication

This class also provides rudimentary support for HTTP basic authentication and/or the securer HTTP digest access authentication.

You can enable basic authentication by setting the `$config['rest_auth']` to `'basic'`. The `$config['rest_valid_logins']` directive can then be used to set the usernames and passwords able to log in to your system. The class will automatically send all the correct headers to trigger the authentication dialogue:

	$config['rest_valid_logins'] = array( 'username' => 'password', 'other_person' => 'secure123' );

Enabling digest auth is similarly easy. Configure your desired logins in the config file like above, and set `$config['rest_auth']` to `'digest'`. The class will automatically send out the headers to enable digest auth.

Both methods of authentication can be secured further by using an IP whitelist. If you enable `$config['rest_ip_whitelist_enabled']` in your config file, you can then set a list of allowed IPs.

Any client connecting to your API will be checked against the whitelisted IP array. If they're on the list, they'll be allowed access. If not, sorry, no can do hombre. The whitelist is a comma-separated string:

	$config['rest_ip_whitelist'] = '123.456.789.0, 987.654.32.1';

Your localhost IPs (`127.0.0.1` and `0.0.0.0`) are allowed by default.

## Contributions

This project was originally written by the awesome Phil Sturgeon, however his involvment has shifted 
as he is no longer using it.  As of 11/20/2013 further developement and support will be done by Chris Kacerguis.

Pull Requests are the best way to fix bugs or add features. I know loads of you use this, so please 
contribute if you have improvements to be made.
