<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** This can be removed if you use __autoload() in config.php OR use Modular Extensions */
require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }
    
    /************
    | Login API |
    *************/

    function login_post()
    {
        /** Throw error if any of required fields are missing */
        if(!$this->post('username')||!$this->post('password')) {
            $message = array("error"=>1,"message"=>"Missing fields!");
            $this->response($message, 400);
        } else {
            
            /** Getting required params  */
            $dataArray = array(
                    'username' => $this->post('username'),
                    'password' => md5($this->post('password'))
                );

            /** Validating the login */
            $data = $this->user_model->validateLogin($dataArray);

            /** Throw response according to codes */
            if($data['code'] == 404) {

                $message = array("error"=>1,"message"=>"User not found!");
                $this->response($message, 404); // 404 being the HTTP response code for Not found

            } elseif ($data['code'] == 401) {
                
                $message = array("error" => 1,"message" =>"Invalid Credentials!");
                $this->response($message, 401); // 401 being the HTTP response code for Bad request

            } else {
                
                $message = array("error" => 0,"message" =>"You have logged in successfully!","token" => $data['auth_token']);
                $this->response($message, 200); // 200 being the HTTP response code for success

            }
        }
    }

    /*************
    | Sign Up API |
    **************/

    function signup_post()
    {
        /** Throw error if any of required fields are missing */
        if(!$this->post('first_name')||!$this->post('last_name')||!$this->post('email')||!$this->post('password')||!$this->post('username')) {
            
            $message = array("error"=>1,"message"=>"Missing fields!");
            $this->response($message, 400);

        } else {
            
            /** Getting required params  */
            $dataArray = array(
                'first_name' => $this->post('first_name'),
                'last_name' => $this->post('last_name'),
                'email' => $this->post('email'),
                'password' => md5($this->post('password')),
                'username' => $this->post('username'),
                'token' => crypt(round(microtime(true) * 1000)."","agile"),
            );

            /** Inserting the data */
            $result = $this->user_model->insertUserData($dataArray);
            
            /** Throw response according to codes */ 
            if($result['code'] == 201) {

                $message = array("error"=>0,"message"=>"Account created!");
                $this->response($message, $result['code']);

            } elseif($result['code'] == 500) {
                
                $message = array("error"=>1,"message"=>"Error while signing up!");
                $this->response($message, $result['code']); // 500 being the HTTP response code for Internal error (in case data is not inserted but provided correctly)

            } elseif ($result['code'] == 409) {
                
                $message = array("error"=>1,"message"=>"A user with the same username already exists! Please try again with different username!");
                $this->response($message, $result['code']); // 409 being the HTTP response code for conflict (if user with the same username already exists)

            }
        }
    }
    

}