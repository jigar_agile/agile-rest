<?php

class user_model extends CI_Model {

	/** Defining parent constructor */
	function __consruct() {
		parent::__consruct();
	}

	/** Returns Count Rows based on condition array provided */
	function countRows($table=NULL,$conditionArray=NULL) {
		if($table == NULL) {
			exit("Table name is required!");
		}
		if($conditionArray == NULL) {
			$query = $this->db->get($table);							/** Using CI `get` method */
			return $query->num_rows();									/** Return rows */
		} else {
			$query = $this->db->get_where($table,$conditionArray);		/** Using CI `get_where` method */
			return $query->num_rows();									/** Return rows */
		}
	}

	function validateLogin($dataArray) {
		
		/** Check if user exists or not */
		$username = $dataArray['username']; 
		$query = $this->db->get_where('auth_user',array("username" => $username));
		if($query->num_rows() == 0) {
			return array("code"=>404);
		} else {
			$query = $this->db->get_where('auth_user',$dataArray);
			if($query->num_rows()==1) {
				$row = $query->first_row();

				/** Update the token */
				$token=crypt(round(microtime(true) * 1000)."","agile");
				$this->db->where('username', $username);
				$this->db->update('auth_user', array("token"=>$token));
				
				/** Return new token */
				return array("code"=>200,"auth_token"=>$token);
			} else {
				return array("code"=>401);
			}
		}
			
	}

	/** General Data Insertion function - Returns Inserted Row ID */
	function insertUserData($dataArray=NULL) {
		$username = $dataArray['username'];
		$query = $this->db->get_where('auth_user',array("username" => $username));
		if($query->num_rows() == 0) {
			$query = $this->db->insert('auth_user', $dataArray);
			$id = $this->db->insert_id();
			if($id) {
				return array("code"=>201,"user_id"=>$id);
			} else {
				return array("code"=>500);
			}
		} else {
			return array("code"=>409);
		}
	}
}